cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/com.chariotsolutions.nfc.plugin/www/phonegap-nfc.js",
        "id": "com.chariotsolutions.nfc.plugin.NFC",
        "runs": true
    },
    {
        "file": "plugins/com.chariotsolutions.nfc.plugin/www/phonegap-nfc-blackberry.js",
        "id": "com.chariotsolutions.nfc.plugin.NFCBB10",
        "runs": true
    },
    {
        "file": "plugins/org.apache.cordova.device/www/device.js",
        "id": "org.apache.cordova.device.device",
        "clobbers": [
            "device"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.dialogs/www/notification.js",
        "id": "org.apache.cordova.dialogs.notification",
        "merges": [
            "navigator.notification"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.dialogs/www/blackberry10/beep.js",
        "id": "org.apache.cordova.dialogs.beep",
        "clobbers": [
            "window.navigator.notification.beep"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.file/www/DirectoryEntry.js",
        "id": "org.apache.cordova.file.DirectoryEntry",
        "clobbers": [
            "window.DirectoryEntry"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.file/www/DirectoryReader.js",
        "id": "org.apache.cordova.file.DirectoryReader",
        "clobbers": [
            "window.DirectoryReader"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.file/www/Entry.js",
        "id": "org.apache.cordova.file.Entry",
        "clobbers": [
            "window.Entry"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.file/www/File.js",
        "id": "org.apache.cordova.file.File",
        "clobbers": [
            "window.File"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.file/www/FileEntry.js",
        "id": "org.apache.cordova.file.FileEntry",
        "clobbers": [
            "window.FileEntry"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.file/www/FileError.js",
        "id": "org.apache.cordova.file.FileError",
        "clobbers": [
            "window.FileError"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.file/www/FileReader.js",
        "id": "org.apache.cordova.file.FileReader",
        "clobbers": [
            "window.FileReader"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.file/www/FileSystem.js",
        "id": "org.apache.cordova.file.FileSystem",
        "clobbers": [
            "window.FileSystem"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.file/www/FileUploadOptions.js",
        "id": "org.apache.cordova.file.FileUploadOptions",
        "clobbers": [
            "window.FileUploadOptions"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.file/www/FileUploadResult.js",
        "id": "org.apache.cordova.file.FileUploadResult",
        "clobbers": [
            "window.FileUploadResult"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.file/www/FileWriter.js",
        "id": "org.apache.cordova.file.FileWriter",
        "clobbers": [
            "window.FileWriter"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.file/www/Flags.js",
        "id": "org.apache.cordova.file.Flags",
        "clobbers": [
            "window.Flags"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.file/www/LocalFileSystem.js",
        "id": "org.apache.cordova.file.LocalFileSystem",
        "clobbers": [
            "window.LocalFileSystem"
        ],
        "merges": [
            "window"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.file/www/Metadata.js",
        "id": "org.apache.cordova.file.Metadata",
        "clobbers": [
            "window.Metadata"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.file/www/ProgressEvent.js",
        "id": "org.apache.cordova.file.ProgressEvent",
        "clobbers": [
            "window.ProgressEvent"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.file/www/fileSystems.js",
        "id": "org.apache.cordova.file.fileSystems"
    },
    {
        "file": "plugins/org.apache.cordova.file/www/requestFileSystem.js",
        "id": "org.apache.cordova.file.requestFileSystem",
        "clobbers": [
            "window.requestFileSystem"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.file/www/resolveLocalFileSystemURI.js",
        "id": "org.apache.cordova.file.resolveLocalFileSystemURI",
        "merges": [
            "window"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/FileProxy.js",
        "id": "org.apache.cordova.file.FileProxy",
        "runs": true
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/info.js",
        "id": "org.apache.cordova.file.bb10FileSystemInfo",
        "runs": true
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/createEntryFromNative.js",
        "id": "org.apache.cordova.file.bb10CreateEntryFromNative",
        "runs": true
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/requestAnimationFrame.js",
        "id": "org.apache.cordova.file.bb10RequestAnimationFrame",
        "runs": true
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/FileSystem.js",
        "id": "org.apache.cordova.file.bb10FileSystem",
        "merges": [
            "window.FileSystem"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.file/www/fileSystems-roots.js",
        "id": "org.apache.cordova.file.fileSystems-roots",
        "runs": true
    },
    {
        "file": "plugins/org.apache.cordova.file/www/fileSystemPaths.js",
        "id": "org.apache.cordova.file.fileSystemPaths",
        "merges": [
            "cordova"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/copyTo.js",
        "id": "org.apache.cordova.file.copyToProxy"
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/getDirectory.js",
        "id": "org.apache.cordova.file.getDirectoryProxy"
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/getFile.js",
        "id": "org.apache.cordova.file.getFileProxy"
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/getFileMetadata.js",
        "id": "org.apache.cordova.file.getFileMetadataProxy"
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/getMetadata.js",
        "id": "org.apache.cordova.file.getMetadataProxy"
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/getParent.js",
        "id": "org.apache.cordova.file.getParentProxy"
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/moveTo.js",
        "id": "org.apache.cordova.file.moveToProxy"
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/readAsArrayBuffer.js",
        "id": "org.apache.cordova.file.readAsArrayBufferProxy"
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/readAsBinaryString.js",
        "id": "org.apache.cordova.file.readAsBinaryStringProxy"
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/readAsDataURL.js",
        "id": "org.apache.cordova.file.readAsDataURLProxy"
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/readAsText.js",
        "id": "org.apache.cordova.file.readAsTextProxy"
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/readEntries.js",
        "id": "org.apache.cordova.file.readEntriesProxy"
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/remove.js",
        "id": "org.apache.cordova.file.removeProxy"
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/removeRecursively.js",
        "id": "org.apache.cordova.file.removeRecursivelyProxy"
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/resolveLocalFileSystemURI.js",
        "id": "org.apache.cordova.file.resolveLocalFileSystemURIProxy"
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/requestAllFileSystems.js",
        "id": "org.apache.cordova.file.requestAllFileSystemsProxy"
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/requestFileSystem.js",
        "id": "org.apache.cordova.file.requestFileSystemProxy"
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/setMetadata.js",
        "id": "org.apache.cordova.file.setMetadataProxy"
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/truncate.js",
        "id": "org.apache.cordova.file.truncateProxy"
    },
    {
        "file": "plugins/org.apache.cordova.file/www/blackberry10/write.js",
        "id": "org.apache.cordova.file.writeProxy"
    },
    {
        "file": "plugins/org.apache.cordova.globalization/www/GlobalizationError.js",
        "id": "org.apache.cordova.globalization.GlobalizationError",
        "clobbers": [
            "window.GlobalizationError"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.globalization/www/globalization.js",
        "id": "org.apache.cordova.globalization.globalization",
        "clobbers": [
            "navigator.globalization"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.media/www/MediaError.js",
        "id": "org.apache.cordova.media.MediaError",
        "clobbers": [
            "window.MediaError"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.media/www/Media.js",
        "id": "org.apache.cordova.media.Media",
        "clobbers": [
            "window.Media"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.network-information/www/network.js",
        "id": "org.apache.cordova.network-information.network",
        "clobbers": [
            "navigator.connection",
            "navigator.network.connection"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.network-information/www/Connection.js",
        "id": "org.apache.cordova.network-information.Connection",
        "clobbers": [
            "Connection"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.splashscreen/www/splashscreen.js",
        "id": "org.apache.cordova.splashscreen.SplashScreen",
        "clobbers": [
            "navigator.splashscreen"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "com.chariotsolutions.nfc.plugin": "0.6.1",
    "org.apache.cordova.console": "0.2.14-dev",
    "org.apache.cordova.device": "0.3.1-dev",
    "org.apache.cordova.dialogs": "0.3.1-dev",
    "org.apache.cordova.file": "1.3.3",
    "org.apache.cordova.globalization": "0.3.5-dev",
    "org.apache.cordova.media": "0.2.17-dev",
    "org.apache.cordova.network-information": "0.2.16-dev",
    "org.apache.cordova.splashscreen": "1.0.1-dev"
}
// BOTTOM OF METADATA
});