/**
* traducción al Ingles  de la aplicación pomodoro
* by Benedetta
*/
var tituloInicial = 'Pomodoro';
var tituloCorto = 'Short break';
var tituloLargo = 'Long break';
var tituloPomodoro = 'Pomodoro started';
var tituloParar = 'Pomodoro stopped';
var tituloFinal = 'Pomodoro finished';
var labelVibracion = 'Number of vibrations';
var configuracion = 'Configuration';
var labelTiempos = 'Times';
var acercaDe = 'about';
var errorAcelerometro = 'Accelerometer error';
var descarga = 'Download version PRO';
var botonCorta = 'Short';
var botonLarga = 'Long';
var botonParar = 'Stop';
var botonAceptar = 'Accept';
var botonCancelar = 'Cancel';
var minutos = "Minutes";
var programador = "programmer";
var diseno = "design";
var traduccion = "translation";
var volver = "Return";
var textoCompartir = "I also use Pomodoro for BB10. http://appworld.blackberry.com/webstore/content/20398621";
var tituloCompartir = "Share your Pomodoro";
var compartir = "Share";
app.traducir();