/**
 * funciones.js funciones de hermes rutas online lite
 * 
 * @projectDescription    Libreria Javascript para pomodoro.
 * @author Juan Luis Estevez juanlu@mosaico-web.com.com
 * @version 1.0
 * @copyright Mosaico-web 2015. 
 */


var concha = {
    cancion: [
        {titulo: 'Nana de la cebolla', cancion: 'Nana de la cebolla.mp3', letra: 'La cebolla es escarcha<br />cerrada y pobre.<br />Escarcha de tus días<br />y de mis noches.<br />Hambre y cebolla,<br />hielo negro y escarcha<br />grande y redonda.<br /><br />En la cuna del hambre<br />mi niño estaba.<br />Con sangre de cebolla<br />se amamantaba.<br />Pero tu sangre,<br />escarchada de azúcar,<br />cebolla y hambre.<br /><br />Una mujer morena<br />resuelta en luna<br />se derrama hilo a hilo<br />sobre la cuna.<br />Ríete, niño,<br />que te traigo la luna<br />cuando es preciso.<br /><br />Alondra de mi casa,<br />ríete mucho.<br />Es tu risa en tus ojos<br />la luz del mundo.<br />Ríete tanto<br />que mi alma al oírte<br />bata el espacio.<br /><br />Tu risa me hace libre,<br />me pone alas.<br />Soledades me quita,<br />cárcel me arranca.<br />Boca que vuela,<br />corazón que en tus labios<br />relampaguea.<br /><br />Es tu risa la espada<br />más victoriosa,<br />vencedor de las flores<br />y las alondras<br />Rival del sol.<br />Porvenir de mis huesos<br />y de mi amor.<br /><br />La carne aleteante,<br />súbito el párpado,<br />el vivir como nunca<br />coloreado.<br />&#161;Cuánto jilguero<br />se remonta, aletea,<br />desde tu cuerpo!<br /><br />Desperté de ser niño:<br />nunca despiertes.<br />Triste llevo la boca:<br />ríete siempre.<br />Siempre en la cuna,<br />defendiendo la risa<br />pluma por pluma.<br /><br />Ser de vuelo tan lato,<br />tan extendido,<br />que tu carne es el cielo<br />recién nacido.<br />&#161;Si yo pudiera<br />remontarme al origen<br />de tu carrera!<br /><br />Al octavo mes ríes<br />con cinco azahares.<br />Con cinco diminutas<br />ferocidades.<br />Con cinco dientes<br />como cinco jazmines<br />adolescentes.<br /><br />Frontera de los besos<br />serán mañana,<br />cuando en la dentadura<br />sientas un arma.<br />Sientas un fuego<br />correr dientes abajo<br />buscando el centro.<br /><br />Vuela niño en la doble<br />luna del pecho:<br />él, triste de cebolla,<br />tú, satisfecho.<br />No te derrumbes.<br />No sepas lo que pasa ni<br />lo que ocurre. <br />'},
        {titulo: 'Nana de Sevilla', cancion: 'nana_de_sevilla.mp3', letra: 'Este galapaguito no tiene mare.<br />No tiene mare, sí<br />no tiene mare, no.<br />no tiene mare<br />Lo parió una gitana, lo echó a la calle.<br />Lo echó a la calle, sí<br />lo echó a la calle, no.<br />lo echó a la calle<br /><br />Este niño chiquito no tiene cuna.<br />No tiene cuna, sí<br />no tiene cuna, no.<br />no tiene cuna<br /><br />Su padre es carpintero y le hará una.<br />Y le hará una, sí<br />y le hará una, no.<br>y le hará una'},
        {titulo: 'Minue', cancion: 'minue.wav', letra: ''},
        {titulo: 'Nana para un niño indigena', cancion: 'nananinoindiguena.wav', letra: 'mi niño eterno, dueño del mundo, <br>mi corazón. <br>Despertarás y habrá acabado la larga noche <br>y su terror. <br>Vendrá la luz y el amanecer posará en tus labios <br>la esperanza que sueñan los pueblos originarios.<br>Sueña Pichiche<br>con las praderas donde el manzano <br>ya floreció, <br>en esa tierra en que el huinca aprende <br>nuestros amores, los que olvidó. <br>Él allí comprenderá que tu gente quiera romper <br>las alambradas que cierran la ruta a Peumayen3. <br>Duerme, mi pequeño, <br>que en el país al que vas dormido <br>escriben la verdadera historia los vencidos <br>No temas despertarte, <br>que la luz que se cuela por el tamiz de tus sueños <br>alumbra esta noche y limpia el cielo del mundo. <br>Duérmete y que vuestro sueño custodie el futuro. <br>Duerme mi wawa<br>la Pachamama5 besa tu frente y en su interior <br>guarda su oro negro y volátil, para ofrecértelo a ti, mi amor. <br>Duerme que un sueño nos salvará de tanto olvido, <br>y espantará al águila que acecha al puma herido. <br>Dulce paal <br>duerme tranquilo, que aquí a la selva no llegarán <br>el monstruo con dientes de acero, rencor y escamas y su ley marcial, <br>que a la tarde llegó un mensajero con pasamontañas <br>diciendo que traerá música y flores por la mañana. <br>Duerme mi pequeño...mi niño eterno, dueño del mundo, <br>mi corazón. <br>Despertarás y habrá acabado la larga noche <br>y su terror. <br>Vendrá la luz y el amanecer posará en tus labios <br>la esperanza que sueñan los pueblos originarios. <br>Sueña Pichiche<br>con las praderas donde el manzano <br>ya floreció, <br>en esa tierra en que el huinca aprende <br>nuestros amores, los que olvidó. <br>Él allí comprenderá que tu gente quiera romper <br>las alambradas que cierran la ruta a Peumayen3. <br>Duerme, mi pequeño, <br>que en el país al que vas dormido <br>escriben la verdadera historia los vencidos <br>No temas despertarte, <br>que la luz que se cuela por el tamiz de tus sueños <br>alumbra esta noche y limpia el cielo del mundo. <br>Duérmete y que vuestro sueño custodie el futuro. <br>Duerme mi wawa<br>la Pachamama5 besa tu frente y en su interior <br>guarda su oro negro y volátil, para ofrecértelo a ti, mi amor. <br>Duerme que un sueño nos salvará de tanto olvido, <br>y espantará al águila que acecha al puma herido. <br>Dulce paal <br>duerme tranquilo, que aquí a la selva no llegarán <br>el monstruo con dientes de acero, rencor y escamas y su ley marcial, <br>que a la tarde llegó un mensajero con pasamontañas <br>diciendo que traerá música y flores por la mañana. <br>Duerme mi pequeño...'},
        {titulo: 'Nana cinco lobitos', cancion: 'nanacincolobitos.wav', letra: ''},
        {titulo: 'Nana del perejil', cancion: 'nanadelperejil.wav', letra: 'A la nana coco <br>del perejil<br>se fue la luna,<br>el cielo gris. <br>A la nana coco,<br>la hierba buena ,<br>duerme mi niño.<br>¡tu madre vela!...'}
    ],
    audio: null,
    tema: null,
    espacioLibre: null,
    plataforma: null,
    temporizador: null,
    inicio: function () {
        concha.plataforma = device.platform;
        concha.redimensionarInicio();
        concha.eventos();
        concha.iniciaNFC();
    },
    eventos: function () {
        $('.cancion').click(concha.eligeTema);
        $('.botonera').click(concha.controlBotonera);
    },
    eligeTema: function (evento) {
        tema = parseInt(evento.currentTarget.id);
        concha.iniciaMusica();
    },
    iniciaMusica: function () {
        $('#cancion').html(concha.cancion[tema]['titulo']);
        $('#letrasCancion').html(concha.cancion[tema]['letra']);
        $('#principal').hide();
        $('#letras').show();
        var controles = $('#contieneControles').height();
        $('#contieneLetra').height(concha.espacioLibre - controles - 20);
        var directorio;
        if (concha.plataforma === "Android") {
            directorio = "www/musica/";
        } else if (concha.plataforma === "blackberry10") {
            directorio = "musica/";
        } else {
            directorio = "www/musica/";
        }
        concha.audio = new Media(cordova.file.applicationDirectory + directorio + concha.cancion[tema]['cancion'], concha.audioFinal, concha.audioError, concha.audioEstado);
        concha.audio.play();
        concha.temporizador = setInterval(concha.progreso, 500);
    },
    audioEstado: function (a) {
        var hola = a;
    },
    audioFinal: function () {
        clearInterval(concha.temporizador);
        concha.volverInicio();
    },
    audioError: function (a, b, c) {
        var hola = a + b + c;
    },
    controlBotonera: function (evento) {
        var funcion = evento.currentTarget.id;
        if (funcion === 'play') {
            concha.audio.play();
        } else if (funcion === 'parar') {
            concha.concha.paraMusica();
        } else if (funcion === 'pausa') {
            concha.audio.pause();
        }
    },
    paraMusica: function () {
        concha.audio.stop();
        clearInterval(concha.temporizador);
        concha.volverInicio();
    },
    progreso: function () {
        var duracion = concha.audio.getDuration();
        concha.audio.getCurrentPosition(function (a) {
            var posicion = a;
            var porcentaje = (duracion / posicion) * 100;
            $('#duracion').css('width', porcentaje + "%");
            if (porcentaje >= 100) {
                concha.paraMusica();
            }
        });

    },
    iniciaNFC: function () {
        nfc.addNdefListener(concha.NFClee, concha.NFCok, concha.NFCfallo);
    },
    NFClee: function (etiqueta) {
        var tag = etiqueta.tag,
                mensaje = tag.ndefMessage[0],
                valor;
        var tipo = nfc.bytesToString(mensaje.type),
                texto;

        if (tipo === "T") {
            var langCodeLength = mensaje.payload[0],
                    text = mensaje.payload.slice((1 + langCodeLength), mensaje.payload.length);
            texto = nfc.bytesToString(text);
            texto = texto.split(',');
            if (texto[0] === "conchaBaby") {
                tema = parseInt(texto[1]);
                concha.iniciaMusica();
            }
        }
    },
    NFCok: function () {
        console.log('NFC Ok');
    },
    NFCfallo: function (fallo) {
        console.log(JSON.stringify(fallo));
    },
    volverInicio: function () {
        concha.audio.stop();
        $('#principal').show();
        $('#letras').hide();
    },
    redimensionarInicio: function () {
        var barra = $('.navbar-fixed').height();
        var pantalla = screen.height; //$('body').height();        
        concha.espacioLibre = pantalla - barra - 120;
        $('.fila').height(concha.espacioLibre / 3);
    }
};

document.addEventListener("deviceready", concha.inicio, false);
