/**
* traducción al Aleman de la aplicación pomodoro
* by Begoña Lourenço Míguez
*/
var tituloInicial = 'Pomodoro';
var tituloCorto = 'kurze Pause';
var tituloLargo = 'lange Pause';
var tituloPomodoro = 'Pomodoro gestartet';
var tituloParar = 'Pomodoro gestoppt';
var tituloFinal = 'Pomodoro beendet';
var labelVibracion = 'Vibrationen Nummer';
var configuracion = 'Konfiguration';
var labelTiempos = 'Zeiten';
var acercaDe = 'Über';
var errorAcelerometro = 'Beschleunigungsmesser Fehler';
var descarga = 'Download Version PRO';
var botonCorta = 'Kurze';
var botonLarga = 'Lange';
var botonParar = 'Stoppen';
var botonAceptar = 'Akzeptieren';
var botonCancelar = 'Abbrechen';
var minutos = "Minuten";
var programador = "programmierer";
var diseno = "design";
var traduccion = "übersetzung";
var volver = "Rückkehr";
var textoCompartir = "Ich auch BB10 Pomodoro verwenden. http://appworld.blackberry.com/webstore/content/20398621";
var tituloCompartir = "Teile deine Pomodoro";
var compartir = "Teile";
app.traducir();