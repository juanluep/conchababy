/**
* traducción al castellano  de la aplicación pomodoro
* by Juanlu
*/
var tituloInicial = 'Pomodoro';
var tituloCorto = 'Pausa corta';
var tituloLargo = 'Pausa larga';
var tituloPomodoro = 'Pomodoro iniciado';
var tituloParar = 'Pomodoro detenido';
var tituloFinal = 'Pomodoro finalizado';
var labelVibracion = 'Número de vibraciones';
var configuracion = 'Configuración';
var labelTiempos = 'Tiempos';
var acercaDe = 'acerca de';
var errorAcelerometro = 'Error de acelerometro';
var descarga = 'Descarga la versión PRO';
var botonCorta = 'Corta';
var botonLarga = 'Larga';
var botonParar = 'Parar';
var botonAceptar = 'Aceptar';
var botonCancelar = 'Cancelar';
var minutos = "Minutos";
var programador = "programación";
var diseno = "diseño";
var traduccion = "traducción";
var volver = "Volver";
var textoCompartir = "Yo tambien uso Pomodoro para BB10. http://appworld.blackberry.com/webstore/content/20398621";
var tituloCompartir = "Comparte tu Pomodoro";
var compartir = "Comparte";
app.traducir();
