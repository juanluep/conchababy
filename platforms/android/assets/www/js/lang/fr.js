/**
* traducción al Frances de la aplicación pomodoro
* by Benedetta
*/
var tituloInicial = 'Pomodoro';
var tituloCorto = 'Pause courte';
var tituloLargo = 'Pause longue';
var tituloPomodoro = 'Pomodoro demarré';
var tituloParar = 'Pomodoro arrêté';
var tituloFinal = 'Pomodoro terminé';
var labelVibracion = 'Nombre de vibrations';
var configuracion = 'Configuration';
var labelTiempos = 'Temps';
var acercaDe = 'sur';
var errorAcelerometro = "Erreur de l'accéléromètre";
var descarga = 'Télécharger la version PRO';
var botonCorta = 'Courte';
var botonLarga = 'Longue';
var botonParar = 'Arrêter';
var botonAceptar = 'Valider';
var botonCancelar = 'Annuler';
var minutos = "Minutes";
var programador = "programmeur";
var diseno = "conception";
var traduccion = "traduction";
var volver = "Retour";
var textoCompartir = "J'utilise aussi Pomodoro pour BB10. http://appworld.blackberry.com/webstore/content/20398621";
var tituloCompartir = "Partagez votre Pomodoro";
var compartir = "Partagez";
app.traducir();