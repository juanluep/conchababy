/**
* traducción al Portuges de la aplicación pomodoro
* by 
*/
var tituloInicial = 'Pomodoro';
var tituloCorto = 'Pausa curta';
var tituloLargo = 'Pausa longa';
var tituloPomodoro = 'Pomodoro iniciado';
var tituloParar = 'Pomodoro detido';
var tituloFinal = 'Pomodoro finalizado';
var labelVibracion = 'Número de vibrações';
var configuracion = 'Configuração';
var labelTiempos = 'Tempos';
var acercaDe = 'Sobre';
var errorAcelerometro = 'Erro de acelerômetro';
var descarga = 'Descarga versão PRO';
var botonCorta = 'Curta';
var botonLarga = 'Longa';
var botonParar = 'Parar';
var botonAceptar = 'Aceitar';
var botonCancelar = 'Cancelar';
var minutos = "Minutos";
var programador = "programador";
var diseno = "projeto";
var traduccion = "tradução";
var volver = "Voltar";
var textoCompartir = "Eu também uso Pomodoro para BB10. http://appworld.blackberry.com/webstore/content/20398621";
var tituloCompartir = "Partilhe a sua Pomodoro";
var compartir = "Partilhe";
app.traducir();