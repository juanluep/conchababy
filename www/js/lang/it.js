/**
* traducción al Italiano de la aplicación pomodoro
* by Benedetta
*/
var tituloInicial = 'Pomodoro';
var tituloCorto = 'Pausa corta';
var tituloLargo = 'Pausa lunga';
var tituloPomodoro = 'Pomodoro avviato';
var tituloParar = 'Pomodoro fermato';
var tituloFinal = 'Pomodoro finito';
var labelVibracion = 'Numero di vibrazioni';
var configuracion = 'Configurazione';
var labelTiempos = 'Tempi';
var acercaDe = 'riguardo a';
var errorAcelerometro = 'Errore di accelerometro';
var descarga = 'Scarica la versione PRO';
var botonCorta = 'Corta';
var botonLarga = 'Lunga';
var botonParar = 'Fermare';
var botonAceptar = 'Accettare';
var botonCancelar = 'Cancellare';
var minutos = "Minuti";
var programador = "programmatore";
var diseno = "design";
var traduccion = "traduzione";
var volver = "Ritorno";
var textoCompartir = "Io uso anche Pomodoro per BB10. http://appworld.blackberry.com/webstore/content/20398621";
var tituloCompartir = "Condividi la tua Pomodoro";
var compartir = "Condividi";
app.traducir();